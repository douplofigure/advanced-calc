#Falgs 'n stuff
CFLAGS:=-g
PROGNAME:=symcalc2

#File-lists
# -not -path "src/main.cpp"
C_FILES :=$(shell find src/ -type f -regex ".*\(\.c\|\.cpp\|\.cc\)" -not -path "src/*.parser.c" -not -path "src/*.scanner.c" | tr "\n" " ")
L_FILES :=$(shell find src/ -type f -regex ".*\(\.l\)"  | tr "\n" " ")
Y_FILES :=$(shell find src/ -type f -regex ".*\(\.y\)"  | tr "\n" " ")


#Advanced Configuration

CC := gcc
CXX := g++
LD := ld
AR := ar
LEX := lex
YACC := yacc

#Targets

#get_dependencies := $(wordlist 2,1024,$(subst :, ,$(shell g++ -MM -I ./src ${1})))
obj_target = obj/${2}/$(addsuffix .o,$(basename ${1}))
lexer_target = $(basename ${1}).scanner.c
parser_target = $(basename ${1}).parser.c

define obj
  $(call obj_target,${1},${2}) : $(wordlist 2,1024,$(subst :, ,$(shell g++ -MM -I ./src ${1} | python3 cleanstrings.py))) | obj/${2}/
  obj$(suffix ${1}) += $(call obj_target,${1},${2})
endef

define lexer
  $(call lexer_target,${1},${2}) : ${1} |
  scanner$(suffix ${1}) += $(call lexer_target,${1},${2})
  C_FILES += $(call lexer_target,${1},${2})
endef

define parser
  $(call parser_target,${1},${2}) : ${1} |
  parser$(suffix ${1}) += $(call parser_target,${1},${2})
  C_FILES += $(call parser_target,${1},${2})
endef

define SOURCES
  $(foreach src,${1},$(eval $(call obj,${src},${2})))
endef

define LEXERS
  $(foreach src,${1},$(eval $(call lexer,${src},${2})))
endef

define PARSERS
  $(foreach src,${1},$(eval $(call parser,${src},${2})))
endef

Debug: bin/Debug/${PROGNAME}
Release: bin/Release/${PROGNAME}
Library: bin/lib/lib${PROGNAME}.a

$(eval $(call LEXERS,${L_FILES},Debug))
$(eval $(call PARSERS,${Y_FILES},Debug))
$(eval $(call SOURCES,${C_FILES},Debug))

O_FILES:=$(foreach src,${C_FILES},$(call obj_target,${src},Debug))
LIBRARY_C_FILES := $(filter-out src/main.cpp,${C_FILES})
LIBRARY_O_FILES:=$(foreach src,${LIBRARY_C_FILES},$(call obj_target,${src},Debug))

bin/Debug/${PROGNAME}: ${O_FILES} |
	@mkdir -p bin/Debug
	$(CXX) -o $@ ${O_FILES}

bin/Release/${PROGNAME} : ${O_FILES} |
	@mkdir -p bin/Release
	$(CXX) -o $@ ${O_FILES}

bin/lib/lib${PROGNAME}.a : ${LIBRARY_O_FILES} |
	@mkdir -p bin/lib
	$(AR) -rcs $@ $^

${scanner.l} : % :
	$(LEX) -o $@ $^

${parser.y} : % :
	$(YACC) -v -d $^ -o $@

${obj.c} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	$(CC) -c -o $@ $< $(CFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

${obj.cpp} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

${obj.cc} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -I./src -I./include/${LINUX_DIR} -I./include/${LINUX_DIR}/python

obj/Debug/:
	@mkdir -p obj/Debug

cleanDebug:
	@rm -r obj

cleanRelease:
	@rm -r obj
