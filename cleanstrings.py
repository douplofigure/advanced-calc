import re
import sys

data = ""
for l in sys.stdin.readlines():
    data += l

data = re.sub(r" \\\n ", " ", data)
    
print(data)
