#include <iostream>

#include "api.h"
#include "function.h"
#include "configuration.h"

int main(int argc, char ** argv) {

    calc::init();

    if (argc >= 2) {
        calc::parseFile(argv[1]);
    } else {
        while (true) {
            std::string line;
            std::getline(std::cin, line);

            calc::parseString(line);

        }
    }

    return 0;

}
