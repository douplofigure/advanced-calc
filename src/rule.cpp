#include "rule.h"

#include <unordered_map>
#include <iostream>

#include "expression.h"

std::unordered_set<std::unique_ptr<Rule>> Rule::definedRules;

Rule::Rule(std::shared_ptr<Expression> head, std::shared_ptr<Expression> tail) {

    this->head = head;
    this->tail = tail;

    definedRules.insert(definedRules.begin(), std::unique_ptr<Rule>(this));

}

Rule::~Rule() {
    //dtor
}

bool Rule::matches(std::shared_ptr<Expression> exp) {

    std::unordered_map<std::string, std::shared_ptr<Expression>> correspondance;

    bool b = this->head->matchesForRule(exp, correspondance);

    return b;
}

std::shared_ptr<Expression> Rule::apply(std::shared_ptr<Expression> exp) {

    std::unordered_map<std::string, std::shared_ptr<Expression>> correspondance;

    if (!head->matchesForRule(exp, correspondance))
        throw std::runtime_error("Cannot apply a rule that does not match");

    return tail->rebuildWith(correspondance);

}

std::unordered_set<std::shared_ptr<Expression>> Rule::applyAll(std::shared_ptr<Expression> exp) {

    std::unordered_set<std::shared_ptr<Expression>> res;

    for ( const std::unique_ptr<Rule> & r : definedRules) {

        std::unordered_set<std::shared_ptr<Expression>> neighbors = exp->getNeighbors(r.get(), exp);

        res += neighbors;

    }

    return res;

}
