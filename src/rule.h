#ifndef RULE_H
#define RULE_H

#include <memory>
#include <unordered_set>

#include "expression.h"

class Rule
{
    public:
        Rule(std::shared_ptr<Expression> head, std::shared_ptr<Expression> tail);
        virtual ~Rule();

        virtual bool matches(std::shared_ptr<Expression> exp);
        virtual std::shared_ptr<Expression> apply(std::shared_ptr<Expression> exp);

        static std::unordered_set<std::shared_ptr<Expression>> applyAll(std::shared_ptr<Expression> exp);

    protected:

    private:

        std::shared_ptr<Expression> head;
        std::shared_ptr<Expression> tail;

        static std::unordered_set<std::unique_ptr<Rule>> definedRules;

};

#endif // RULE_H
