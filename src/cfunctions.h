#ifndef CFUNCTIONS_H
#define CFUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
Functions for creating expressions from constants
**/
void * expressionCreateInt(long int value);
void * expressionCreateDouble(double value);
void * expressionCreateSymbol(char * symbol);
void * expressionCreateVector(void * ls);
void * expressionCreateMatrix(void * mat);

void * expressionMarkParenthesis(void * exp);
void * expressionNegate(void * exp);

void * expressionCreateFunction(char * name, void * ls);

void * expressionJoinOperator(char * op, void * exp1, void * exp2);

void symbolDeclare(char * symbol, void * exp);
void functionDeclare(char * symbol, void * args, void * exp);
void * symbolShow(char * symbol);

void * variableListBegin(char * symbol, void * exp);
void * variableListAppend(void * ls, char * symbol, void * exp);

void * expressionListBegin(void * exp);
void * expressionListAppend(void * ls, void * exp);

void * expressionMatrixBegin(void * line);
void * expressionMatrixAppend(void * mat, void * line);

void * expressionCompute(void * exp, void * varList);

void * ruleCreate(void * head, void * tail);

void rulesApply(void * exp);
void * expressionSimplify(void * exp);

void configSetValue(char * name, long int value);
void configEnableOutput();
void configDisableOutput();

void messagePrint(char * str);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // CFUNCTIONS_H
