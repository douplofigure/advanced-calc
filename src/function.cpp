#include "function.h"

#include <iostream>
#include <sstream>
#include <limits>

#include "expression.h"
#include "operator.h"

std::unordered_map<std::string, std::unique_ptr<Function>> Function::definedFunctions;

Function::Function(std::string name, unsigned int argCount, std::string formatString) : Function(name, argCount) {

    this->hasSpecialFormat = true;
    this->formatString = formatString;

}

Function::Function(std::string name, unsigned int argCount) : Function(name, argCount, true) {



}

Function::Function(std::string name, unsigned int argCount, bool b) {

    this->argCount = argCount;
    if (b) {
        definedFunctions[name] = std::unique_ptr<Function>(this);
    }

    this->name = name;
    this->formatString = name;
    this->hasSpecialFormat = false;

}

Function::Function(std::string name, unsigned int argCount, std::string formatString, bool b) {

    this->argCount = argCount;
    if (b) {
        definedFunctions[name] = std::unique_ptr<Function>(this);
    }

    this->name = name;
    this->formatString = formatString;
    this->hasSpecialFormat = true;

}

Function::~Function()
{
    //dtor
}

unsigned int Function::getPrecedence() {
    return std::numeric_limits<unsigned short int>::max();
}

std::string Function::getName() {
    return name;
}

std::shared_ptr<Expression> Function::negateExpression(std::shared_ptr<FunctionExpression> exp) {
    return nullptr;
}

Function * Function::getFunction(std::string name) {

    if (definedFunctions.find(name) == definedFunctions.end())
        throw std::runtime_error("Undefined function");

    return definedFunctions[name].get();

}

std::ostream & Function::writeToStream(std::ostream & stream, std::vector<std::shared_ptr<Expression>> & operands) {

    stream << name << '(';

    for (int i = 0; i < operands.size(); ++i) {
        stream << operands[i];
        if (i < operands.size()-1)
            stream << ',';
    }

    return stream << ')';

}

std::string Function::format(const std::vector<std::shared_ptr<Expression>> & operands) {

    std::stringstream stream;

    if (this->hasSpecialFormat) {

        std::string rest = this->formatString;
        size_t pos = 0;

        while ((pos = rest.find("%")) != std::string::npos) {

            stream << rest.substr(0, pos);

            rest = rest.substr(pos);
            pos = 1;
            while (rest[pos] >= '0' && rest[pos] <= '9') ++pos;

            int argNum = atoi(rest.substr(1, pos).c_str());

            if (argNum >= operands.size() || argNum < 0) {
                throw std::runtime_error("Wrong argument number in formating text.");
            }

            std::shared_ptr<Expression> exp = operands[argNum];
            stream << exp;

            if (pos == std::string::npos)
                break;

            rest = rest.substr(pos);

        }

        stream << rest;

        return stream.str();

    }

    stream << name << "\\left (";

    for (int i = 0; i < operands.size(); ++i) {
        stream << operands[i]->format();
        if (i < operands.size()-1)
            stream << ", ";
    }

    stream << "\\right )";

    return stream.str();

}

Math::dMatrix<Math::Number> Function::call(std::vector<std::shared_ptr<Expression>> args, SymbolTable & table) {

    if (args.size() != this->argCount)
        throw std::runtime_error("Wrong argument count for function or operator");

    return this->evaluate(args, table);

}

BuiltinFunction::BuiltinFunction(std::string name, unsigned int argCount, ComputeFunction f) : Function(name, argCount), func(f) {

    this->complexity = 2;

}

BuiltinFunction::BuiltinFunction(std::string name, unsigned int argCount, std::string formatString, ComputeFunction f) : Function(name, argCount, formatString), func(f) {

    this->complexity = 2;

}

BuiltinFunction::BuiltinFunction(std::string name, unsigned int argCount, int complexity, std::string formatString, ComputeFunction f) : Function(name, argCount, formatString), func(f) {

    this->complexity = complexity;

}

BuiltinFunction::~BuiltinFunction() {

}

Math::dMatrix<Math::Number> BuiltinFunction::evaluate(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable) {

    return this->func(args, symbolTable);

}

int BuiltinFunction::getComplexity() {
    return complexity;
}

std::ostream & operator<<(std::ostream & stream, Function * f) {
    return stream << f->getName();
}

void Function::loadBuiltins() {

    BuiltinOperator * operatorAdd = new BuiltinOperator("+", 2, [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {
        return args[0]->getValue(symbols) + args[1]->getValue(symbols);
    });

    operatorAdd->setNegationFunction([] (std::shared_ptr<FunctionExpression> exp) -> std::shared_ptr<Expression> {

        std::vector<std::shared_ptr<Expression>> args = {exp->getOperands()[0]->negated(exp->getOperands()[0]), exp->getOperands()[1]->negated(exp->getOperands()[1])};
        return std::shared_ptr<Expression>(new FunctionExpression(exp->getOperator(), args));

    });

    BuiltinOperator * operatorSub = new BuiltinOperator("-", 2, [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {
        return args[0]->getValue(symbols) - args[1]->getValue(symbols);
    });

    BuiltinOperator * operatorMult = new BuiltinOperator("*", 3, "%0 \\cdot %1", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {
        return args[0]->getValue(symbols) * args[1]->getValue(symbols);
    });

    operatorMult->setNegationFunction([] (std::shared_ptr<FunctionExpression> exp) -> std::shared_ptr<Expression> {

        std::vector<std::shared_ptr<Expression>> args = {exp->getOperands()[0]->negated(exp->getOperands()[0]), exp->getOperands()[1]};
        return std::shared_ptr<Expression>(new FunctionExpression(exp->getOperator(), args));

    });

    BuiltinOperator * operatorPow = new BuiltinOperator("^", 4, "%0^{ %1 }", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {

        Math::Number res = Math::pow(args[0]->getValue(symbols)[0][0], args[1]->getValue(symbols)[0][0]);
        return Math::dMatrix<Math::Number>(1, 1, &res);
    });

    BuiltinOperator * operatorDiv = new BuiltinOperator("/", 3, "\\frac{%0}{%1}", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {
        Math::Number res = args[0]->getValue(symbols)[0][0] / args[1]->getValue(symbols)[0][0];
        return Math::dMatrix<Math::Number>(1, 1, &res);
    });

    BuiltinOperator * operatorEQ = new BuiltinOperator("=", 1, [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {
        Math::Number res = args[0]->getValue(symbols) == args[1]->getValue(symbols);
        return Math::dMatrix<Math::Number>(1, 1, &res);
    });

    BuiltinFunction * nfFunc = new BuiltinFunction("nf", 1, [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {

        Math::Number one = Math::Number((unsigned long int) 1);
        return args[0]->getValue(symbols) + Math::dMatrix<Math::Number>(1, 1, &one);

    });

    BuiltinFunction * sqrtFunc = new BuiltinFunction("sqrt", 1, 4, "\\sqrt{%0}", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number>{

        Math::Number res = Math::pow(args[0]->getValue(symbols)(0,0), Math::Number((long int)1, (long int)2));
        return Math::dMatrix<Math::Number>(1, 1, &res);

    });

    BuiltinFunction * cosFunc = new BuiltinFunction("cos", 1, 5, "\\cos \\left( %0 \\right)", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number>{

        Math::Number res = Math::Number(cos((double) args[0]->getValue(symbols)(0,0)));
        return Math::dMatrix<Math::Number>(1, 1, &res);

    });

    BuiltinFunction * sinFunc = new BuiltinFunction("sin", 1, 5, "\\sin \\left( %0 \\right)", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number>{

        Math::Number res = Math::Number(sin((double) args[0]->getValue(symbols)(0,0)));
        return Math::dMatrix<Math::Number>(1, 1, &res);

    });

    BuiltinFunction * tanFunc = new BuiltinFunction("tan", 1, 5, "\\tan \\left( %0 \\right)", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number>{

        Math::Number res = Math::Number(tan((double) args[0]->getValue(symbols)(0,0)));
        return Math::dMatrix<Math::Number>(1, 1, &res);

    });

    BuiltinFunction * acosFunc = new BuiltinFunction("acos", 1, 5, "\\cos^{-1} \\left( %0 \\right)", [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number>{

        Math::Number res = Math::Number(acos((double) args[0]->getValue(symbols)(0,0)));
        return Math::dMatrix<Math::Number>(1, 1, &res);

    });

    BuiltinFunction * complexityFunc = new BuiltinFunction("COMPLEXITY", 1, [] (std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbols) -> Math::dMatrix<Math::Number> {

        int complexity = args[0]->getComplexity();
        Math::Number c = Math::Number(complexity);
        return Math::dMatrix<Math::Number>(1, 1, &c);

    });

    Expression::declareConstant("pi", Math::Number(M_PI), "\\pi");
    Expression::declareConstant("e", Math::Number(exp(1)));
    Expression::declareConstant("i", Math::Number(0.0, 1.0));

    Expression::declareConstant("infty", Math::Number(std::numeric_limits<double>::infinity()), "\\infty");

}

DefinedFunction::DefinedFunction(std::string name, std::vector<std::shared_ptr<Expression>> argNames, std::shared_ptr<Expression> exp) : Function(name, argNames.size()) {

    this->exp = exp;
    this->varNames = std::vector<std::string>(argNames.size());

    for (int i = 0; i < argNames.size(); ++i) {

        SymbolExpression * tmp = dynamic_cast<SymbolExpression*>(argNames[i].get());
        if (!tmp)
            throw std::runtime_error("Non-symbol-expression in function definition");

        varNames[i] = tmp->getSymbolName();

    }

}

DefinedFunction::~DefinedFunction() {

}

int DefinedFunction::getComplexity() {
    return this->exp->getComplexity();
}

Math::dMatrix<Math::Number> DefinedFunction::evaluate(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable) {

    SymbolTable t = symbolTable;

    for (int i = 0; i < this->varNames.size(); ++i) {
        t[varNames[i]] = args[i];
    }

    return this->exp->getValue(t);

}
