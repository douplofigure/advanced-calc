#ifndef API_H
#define API_H

#include <queue>
#include <string>

extern "C" {

int processLexeme(char * str);
int getLexerInput(char * buffer, int max_size);

}

std::queue<char>& operator<<(std::queue<char>& q, std::string str);

namespace calc {

void init();

void parseString(std::string str, bool addSemicolon = false);
void parseFile(const char * fname);

void clearBuffer();
void executeBuffer();
void pushToBuffer(std::string str);

}

#endif // API_H
