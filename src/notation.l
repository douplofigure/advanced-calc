
%option prefix="sym"

%{
#include "notation.parser.h"
#include "cfunctions.h"
#include <locale.h>

extern int getLexerInput(char * buffer, int size);

#define YY_INPUT(buf, result, max_size) {result = getLexerInput(buf, max_size);}

extern int processLexeme(char * str);

%}

%%

"let"                       {return KW_LET;}
"compute"                   {return KW_COMPUTE;}
"with"                      {return KW_WITH;}
"equation"                  {return KW_EQUATION;}
"solve"                     {return KW_SOLVE;}
"rule"                      {return KW_RULE;}
"apply"                     {return KW_APPLY;}
"simplify"                  {return KW_SIMPLIFY;}
"show"                      {return KW_SHOW;}

"config"                    {return KW_CONFIG;}

"exit"                      {return KW_EXIT;}

":="					    {return OP_DEF;}
"->"					    {return OP_TO;}
[0-9]+\.[0-9]+				{setlocale(LC_NUMERIC, "en_US");symlval.d = strtod(symtext, NULL); return CONST_FLOAT;}
\-[0-9]+\.[0-9]+			{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
[0-9]+\.[0-9]+"e"[0-9]+		{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
\-[0-9]+\.[0-9]+"e"[0-9]+	{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
[0-9]+"e"[0-9]+				{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
\-[0-9]+"e"[0-9]+			{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}

\"(\\.|[^"\\])*\"                      {messagePrint(symtext);}

[0-9]+					    {symlval.i = atol(symtext); return CONST_INT;}
\-[0-9]+				    {symlval.i = atol(symtext); return CONST_INT;}
[:\,\;\{\}\(\)\[\]\_\$]	    {return *symtext;}
[\+\-\*\/\^\=\<\>]                        {return processLexeme(symtext);}
[a-zA-Z\_\\\!\$]+[a-zA-Z\_\\\!0-9]*		{return processLexeme(symtext);}

[\t\r\f ]	                {}
"\n"                        {}
%%
