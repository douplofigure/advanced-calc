#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <unordered_map>
#include <string>

namespace Configuration {

    void setDefaultValues();

    void setValue(std::string name, long int value);
    long int getValue(std::string name);

    void enableOutput();
    bool outputEnabled();
    void disableOutput();

}

#endif // CONFIGURATION_H
