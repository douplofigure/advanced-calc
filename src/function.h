#ifndef FUNCTION_H
#define FUNCTION_H

#include <vector>
#include <unordered_map>
#include <memory>

#include "util/number.h"
#include "util/dmatrix.h"

class Expression;
class FunctionExpression;

typedef std::unordered_map<std::string, std::shared_ptr<Expression>> SymbolTable;
typedef std::function<Math::dMatrix<Math::Number>(std::vector<std::shared_ptr<Expression>>, SymbolTable &)> ComputeFunction;

class Function
{
    public:
        Function(std::string name, unsigned int argCount);
        Function(std::string name, unsigned int argCount, std::string formatString);
        virtual ~Function();

        Math::dMatrix<Math::Number> call(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable);

        static Function * getFunction(std::string name);

        std::string getName();

        virtual std::ostream & writeToStream(std::ostream & stream, std::vector<std::shared_ptr<Expression>> & operands);

        virtual unsigned int getPrecedence();
        virtual int getComplexity() = 0;

        virtual std::string format(const std::vector<std::shared_ptr<Expression>> & operands);

        static void loadBuiltins();

        virtual std::shared_ptr<Expression> negateExpression(std::shared_ptr<FunctionExpression> exp);

    protected:

        Function(std::string name, unsigned int argCount, bool reg);
        Function(std::string name, unsigned int argCount, std::string formatString, bool reg);

        virtual Math::dMatrix<Math::Number> evaluate(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable) = 0;


        bool hasSpecialFormat;

    private:

        std::string name;
        std::string formatString;
        unsigned int argCount;

        static std::unordered_map<std::string, std::unique_ptr<Function>> definedFunctions;

};

class BuiltinFunction : public Function {

    public:
        BuiltinFunction(std::string name, unsigned int argCount, ComputeFunction f);
        BuiltinFunction(std::string name, unsigned int argCount, std::string formatString, ComputeFunction f);
        BuiltinFunction(std::string name, unsigned int argCount, int complexity, std::string formatString, ComputeFunction f);
        virtual ~BuiltinFunction();

        int getComplexity();

    protected:
        virtual Math::dMatrix<Math::Number> evaluate(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable);

    private:

        ComputeFunction func;
        int complexity;


};

class DefinedFunction : public Function {

    public:
        DefinedFunction(std::string name, std::vector<std::shared_ptr<Expression>> argNames, std::shared_ptr<Expression> expression);
        virtual ~DefinedFunction();

        int getComplexity();

    protected:
        virtual Math::dMatrix<Math::Number> evaluate(std::vector<std::shared_ptr<Expression>> args, SymbolTable & symbolTable);

    private:

        std::shared_ptr<Expression> exp;
        std::vector<std::string> varNames;


};

std::ostream & operator<<(std::ostream & stream, Function * f);

#endif // FUNCTION_H
