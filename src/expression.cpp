#include "expression.h"

#include <limits>
#include <stack>
#include <sstream>

#include "util/printing.h"
#include "function.h"
#include "operator.h"
#include "rule.h"
#include "configuration.h"

using namespace Math;

std::unordered_map<std::string, std::shared_ptr<Expression>> Expression::symbolTable;
std::unordered_map<std::string, std::string> Expression::formatTable;

Expression::Expression()
{
    //ctor
}

Expression::~Expression()
{
    //dtor
}

std::string Expression::format() {

    std::stringstream buffer;
    this->writeToStream(buffer);
    return buffer.str();

}

void Expression::declareSymbol(std::string name, std::shared_ptr<Expression> exp) {
    symbolTable[name] = exp;

    std::shared_ptr<SymbolExpression> nameExp(new SymbolExpression(name));
    Rule * r1 = new Rule(nameExp, exp);
    Rule * r2 = new Rule(exp, nameExp);

}

void Expression::declareSymbol(std::string name, std::shared_ptr<Expression> exp, std::string format) {
    symbolTable[name] = exp;
    formatTable[name] = format;

    std::shared_ptr<SymbolExpression> nameExp(new SymbolExpression(name));
    Rule * r1 = new Rule(nameExp, exp);
    Rule * r2 = new Rule(exp, nameExp);

}

void Expression::declareConstant(std::string name, Number n) {
    declareConstant(name, dMatrix<Number>(1, 1, &n));
}

std::shared_ptr<Expression> Expression::getDefinition() {
    return nullptr;
}

void Expression::declareConstant(std::string name, dMatrix<Number> n) {

    std::shared_ptr<Expression> exp(new ConstantExpression(n));
    declareSymbol(name, exp);

}

void Expression::declareConstant(std::string name, Number n, std::string format) {
    declareConstant(name, dMatrix<Number>(1, 1, &n), format);
}

void Expression::declareConstant(std::string name, dMatrix<Number> n, std::string format) {

    std::shared_ptr<Expression> exp(new ConstantExpression(n));
    declareSymbol(name, exp, format);

}

bool Expression::hasFormatString(std::string name) {
    return (formatTable.find(name) != formatTable.end());
}

std::shared_ptr<Expression> Expression::negated(std::shared_ptr<Expression> exp) {

    std::shared_ptr<Expression> neg1(new ConstantExpression(Math::Number((long int)-1)));
    std::vector<std::shared_ptr<Expression>> args = {neg1, exp};
    std::shared_ptr<Expression> resExp(new FunctionExpression(Operator::getOperator("*"), args));

    return resExp;

}

std::string Expression::getFormatString(std::string name) {
    if (!hasFormatString(name))
        throw std::runtime_error(std::string("No format defined for ").append(name));
    return formatTable[name];
}

void Expression::setPrecedenceOverride(unsigned int p) {

}

unsigned int Expression::getPrecedence() {
    return std::numeric_limits<unsigned short int>::max();
}

bool Expression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {
    return false;
}

template <typename T> std::unordered_set<T> & setMerge(std::unordered_set<T> & s1, const std::unordered_set<T> & s2) {
    std::unordered_set<T> res = s1;
    for (const T & t: s2)
        s1.insert(s1.end(), t);
    return s1;
}

template <typename T> std::unordered_set<T> & operator+=(std::unordered_set<T> & s1, const std::unordered_set<T> s2) {
    return setMerge(s1, s2);
}

struct SearchFrame {
    std::shared_ptr<Expression> exp;
    int lvl;
    std::shared_ptr<SearchFrame> parent;
};

std::list<std::shared_ptr<Expression>> Expression::step(std::shared_ptr<Expression> exp) {

    std::stack<std::shared_ptr<SearchFrame>> toProcess;

    std::shared_ptr<SearchFrame> initFrame(new SearchFrame());
    initFrame->exp = exp;
    initFrame->lvl = 0;
    initFrame->parent = nullptr;

    toProcess.push(initFrame);

    while (!toProcess.empty()) {

        std::shared_ptr<SearchFrame> current = toProcess.top();
        toProcess.pop();

        if (current->exp->getComplexity() < exp->getComplexity()) {
            std::list<std::shared_ptr<Expression>> res;

            do {

                res.insert(res.begin(), current->exp);

                current = current->parent;

            } while (current->parent);

            return res;

        }

        if (current->lvl >= 3)
            continue;

        std::unordered_set<std::shared_ptr<Expression>> neighbors = Rule::applyAll(current->exp);

        for (const std::shared_ptr<Expression> & e : neighbors) {

            std::shared_ptr<SearchFrame> nFrame(new SearchFrame());
            nFrame->exp = e;
            nFrame->lvl = current->lvl + 1;
            nFrame->parent = current;

            toProcess.push(nFrame);

        }

    }

    return std::list<std::shared_ptr<Expression>>();

}

std::unordered_set<std::shared_ptr<Expression>> Expression::getNeighbors(Rule * rule, std::shared_ptr<Expression> exp) {

    std::unordered_set<std::shared_ptr<Expression>> res;

    if (rule->matches(exp))
        res = {rule->apply(exp)};

    if (exp->isConstant()) {
        std::unordered_map<std::string, std::shared_ptr<Expression>> variables;
        res += {std::shared_ptr<Expression>(new ConstantExpression(exp->getValue(variables)))};
    }

    return res;

}

std::unordered_set<std::shared_ptr<Expression>> FunctionExpression::getNeighbors(Rule * rule, std::shared_ptr<Expression> exp) {

    std::unordered_set<std::shared_ptr<Expression>> res;

    if (rule->matches(exp))
        res = {rule->apply(exp)};

    if (exp->isConstant()) {
        std::unordered_map<std::string, std::shared_ptr<Expression>> variables;
        res += {std::shared_ptr<Expression>(new ConstantExpression(exp->getValue(variables)))};
    }

    for (int i = 0; i < operands.size(); ++i) {

        const std::shared_ptr<Expression> & e = operands[i];

        if (rule->matches(e)) {

            std::unordered_set<std::shared_ptr<Expression>> n = e->getNeighbors(rule, e);
            for (const std::shared_ptr<Expression> & o : n) {

                std::vector<std::shared_ptr<Expression>> nops(operands.size());
                for (int j = 0; j < operands.size(); ++j)
                    if (i != j) nops[j] = operands[j];

                nops[i] = o;

                res += {std::shared_ptr<Expression>(new FunctionExpression(op, nops))};

            }

        }

    }

    return res;

}

std::unordered_set<std::shared_ptr<Expression>> VectorExpression::getNeighbors(Rule * rule, std::shared_ptr<Expression> exp) {

    std::unordered_set<std::shared_ptr<Expression>> res;

    if (rule->matches(exp))
        res += {rule->apply(exp)};

    if (exp->isConstant()) {
        std::unordered_map<std::string, std::shared_ptr<Expression>> variables;
        res += {std::shared_ptr<Expression>(new ConstantExpression(exp->getValue(variables)))};
    }

    for (int i = 0; i < operands.size(); ++i) {

        const std::shared_ptr<Expression> & e = operands[i];

        if (rule->matches(e)) {

            std::unordered_set<std::shared_ptr<Expression>> n = e->getNeighbors(rule, e);
            for (const std::shared_ptr<Expression> & o : n) {

                std::vector<std::shared_ptr<Expression>> nops(operands.size());
                for (int j = 0; j < operands.size(); ++j)
                    if (i != j) nops[j] = operands[j];

                nops[i] = o;

                res += {std::shared_ptr<Expression>(new VectorExpression(nops))};

            }

        }

    }

    return res;

}

std::unordered_set<std::shared_ptr<Expression>> MatrixExpression::getNeighbors(Rule * rule, std::shared_ptr<Expression> exp) {

    std::unordered_set<std::shared_ptr<Expression>> res;

    if (rule->matches(exp))
        res += {rule->apply(exp)};

    if (exp->isConstant()) {
        std::unordered_map<std::string, std::shared_ptr<Expression>> variables;
        res += {std::shared_ptr<Expression>(new ConstantExpression(exp->getValue(variables)))};
    }

    for (int i = 0; i < operands.size(); ++i) {

        const std::shared_ptr<Expression> & e = operands[i];

        if (rule->matches(e)) {

            std::unordered_set<std::shared_ptr<Expression>> n = e->getNeighbors(rule, e);
            for (const std::shared_ptr<Expression> & o : n) {

                std::vector<std::shared_ptr<Expression>> nops(operands.size());
                for (int j = 0; j < operands.size(); ++j)
                    if (i != j) nops[j] = operands[j];

                nops[i] = o;

                res += {std::shared_ptr<Expression>(new MatrixExpression(rows, cols, nops))};

            }

        }

    }

    return res;

}

std::string ConstantExpression::format() {

    return printing::formatMatrix(value);

}

int Expression::getComplexity() {
    return 1;
}

std::shared_ptr<Expression> Expression::getSymbol(std::string name) {
    if (symbolTable.find(name) == symbolTable.end())
        return nullptr;
    return symbolTable[name];
}

ConstantExpression::ConstantExpression(long int i) {
    Number n = Number(i);
    this->value = dMatrix<Number>(1, 1, &n);
}

ConstantExpression::ConstantExpression(double d) {
    Number n = Number(d);
    this->value = dMatrix<Number>(1, 1, &n);
}

ConstantExpression::ConstantExpression(Number n) {
    this->value = dMatrix<Number>(1, 1, &n);
}

ConstantExpression::ConstantExpression(dMatrix<Number> v) {
    this->value = v;
}

std::shared_ptr<Expression> ConstantExpression::negated(std::shared_ptr<Expression> exp) {

    return std::shared_ptr<ConstantExpression>(new ConstantExpression(value * Math::Number(-1)));

}

ConstantExpression::~ConstantExpression() {

}

bool ConstantExpression::isConstant() {
    return true;
}

std::ostream & ConstantExpression::writeToStream(std::ostream & stream) {

    if (Configuration::getValue("tex_print")) {
        return stream << this->format();
    }

    return stream << this->value;
}

bool ConstantExpression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    ConstantExpression * tmp = dynamic_cast<ConstantExpression*>(exp.get());

    return tmp && tmp->value == value;

}

dMatrix<Number> ConstantExpression::getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) {
    return value;
}

std::shared_ptr<Expression> ConstantExpression::rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {
    return std::shared_ptr<Expression>(new ConstantExpression(value));
}

FunctionExpression::FunctionExpression(Function * op, std::vector<std::shared_ptr<Expression>> operands) {
    this->op = op;
    this->operands = operands;
    precendenceOverride = 0;
}

FunctionExpression::~FunctionExpression(){

}

std::shared_ptr<Expression> FunctionExpression::negated(std::shared_ptr<Expression> exp) {

    std::shared_ptr<FunctionExpression> fexp = std::dynamic_pointer_cast<FunctionExpression>(exp);

    if (!fexp)
        throw std::runtime_error("Something went horribly wrong! Negating an expression of wrong type.");

    std::shared_ptr<Expression> tmp = this->op->negateExpression(fexp);

    if (tmp) {
        return tmp;
    }

    return Expression::negated(exp);

}

bool FunctionExpression::isConstant() {

    bool b = true;

    for (std::shared_ptr<Expression> e : this->operands)
        b &= e->isConstant();

    return b;

}

int FunctionExpression::getComplexity() {

    int s = 0;
    for (std::shared_ptr<Expression> e : operands)
        s += e->getComplexity();

    return op->getComplexity() * s;

}

std::ostream & FunctionExpression::writeToStream(std::ostream & stream) {

    if (Configuration::getValue("tex_print")) {
        return stream << this->format();
    }

    return this->op->writeToStream(stream, this->operands);

}

std::string FunctionExpression::format() {

    return this->op->format(operands);

}

unsigned int FunctionExpression::getPrecedence() {

    if (precendenceOverride)
        return precendenceOverride;

    return op->getPrecedence();
}

void FunctionExpression::setPrecedenceOverride(unsigned int p) {
    precendenceOverride = p;
}

std::vector<std::shared_ptr<Expression>> & FunctionExpression::getOperands() {
    return operands;
}

Function * FunctionExpression::getOperator() {
    return op;
}

dMatrix<Number> FunctionExpression::getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) {

    std::vector<std::shared_ptr<Expression>> opData(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        opData[i] = operands[i];//->getValue(variables);
    }

    return op->call(opData, variables);

}

bool FunctionExpression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    FunctionExpression * tmp = dynamic_cast<FunctionExpression*>(exp.get());

    if (!tmp) return false;
    if (tmp->op != op) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i)
        b &= operands[i]->matchesForRule(tmp->operands[i], correspondance);

    return b;

}

std::shared_ptr<Expression> FunctionExpression::rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    std::vector<std::shared_ptr<Expression>> nop(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        nop[i] = operands[i]->rebuildWith(correspondance);
    }

    return std::shared_ptr<Expression>(new FunctionExpression(op, nop));

}

VectorExpression::VectorExpression(std::vector<std::shared_ptr<Expression>> operands) {
    this->operands = operands;
}

VectorExpression::~VectorExpression(){

}

int VectorExpression::getComplexity() {

    int s = 0;
    for (std::shared_ptr<Expression> e : operands)
        s += e->getComplexity();

    return s;

}

bool VectorExpression::isConstant() {

    bool b = true;

    for (std::shared_ptr<Expression> e : this->operands)
        b &= e->isConstant();

    return b;

}

std::ostream & VectorExpression::writeToStream(std::ostream & stream) {

    if (Configuration::getValue("tex_print")) {
        return stream << this->format();
    }

    stream << '[';

    stream << operands[0];
    for (int i = 1; i < this->operands.size(); ++i) {
        stream << ", " << operands[i];
    }

    return stream << ']';

}

std::string VectorExpression::format() {

    std::stringstream stream;

    stream << "\\begin{pmatrix}";
    stream << operands[0]->format();
    for (int i = 1; i < this->operands.size(); ++i) {
        stream << "\\\\" << operands[i]->format();
    }

    stream << "\\end{pmatrix}";

    return stream.str();

}

dMatrix<Number> VectorExpression::getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) {

    std::vector<Number> res(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        res[i] = operands[i]->getValue(variables)(0,0);
    }

    dMatrix<Number> tmp = dMatrix<Number>(operands.size(), 1, res.data());

    return tmp;

}

bool VectorExpression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    VectorExpression * tmp = dynamic_cast<VectorExpression*>(exp.get());

    if (!tmp) return false;
    if (tmp->operands.size() != operands.size()) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= (operands[i]->matchesForRule(tmp->operands[i], correspondance));
    }

    return b;

}

std::shared_ptr<Expression> VectorExpression::rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    std::vector<std::shared_ptr<Expression>> nop(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        nop[i] = operands[i]->rebuildWith(correspondance);
    }

    return std::shared_ptr<Expression>(new VectorExpression(nop));

}

MatrixExpression::MatrixExpression(std::vector<std::vector<std::shared_ptr<Expression>>> operands) {
    this->operands = std::vector<std::shared_ptr<Expression>>(operands[0].size() * operands.size());
    this->cols = operands[0].size();
    this->rows = operands.size();

    int index = 0;

    for (int i = 0; i < rows; ++i) {

        for (int j = 0; j < cols; ++j) {

            this->operands[index++] = operands[i][j];

        }

    }

}

MatrixExpression::MatrixExpression(int rows, int cols, std::vector<std::shared_ptr<Expression>> operands) {
    this->rows = rows;
    this->cols = cols;
    this->operands = operands;
}

MatrixExpression::~MatrixExpression(){

}

bool MatrixExpression::isConstant() {

    bool b = true;

    for (std::shared_ptr<Expression> e : this->operands)
        b &= e->isConstant();

    return b;

}

dMatrix<Number> MatrixExpression::getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) {

    std::vector<Number> res(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        res[i] = operands[i]->getValue(variables)(0,0);
    }

    dMatrix<Number> tmp = dMatrix<Number>(rows, cols, res.data());

    return tmp;

}

int MatrixExpression::getComplexity() {

    int s = 0;
    for (std::shared_ptr<Expression> e : operands)
        s += e->getComplexity();

    return s;

}

std::ostream & MatrixExpression::writeToStream(std::ostream & stream) {

    if (Configuration::getValue("tex_print")) {
        return stream << this->format();
    }

    stream << '[';
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {

            stream << operands[i*cols+j];
            if (j < cols - 1)
                stream << ',';

        }
        if (i < rows - 1)
            stream << ';';
    }

    return stream << ']';

}

std::string MatrixExpression::format() {

    std::stringstream stream;

    stream << "\\begin{pmatrix} ";
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {

            stream << operands[i*cols+j]->format();
            if (j < cols - 1)
                stream << " & ";

        }
        if (i < rows - 1)
            stream << " \\\\ ";
    }

    stream << " \\end{pmatrix}";

    return stream.str();

}

bool MatrixExpression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    MatrixExpression * tmp = dynamic_cast<MatrixExpression*>(exp.get());

    if (!tmp) return false;
    if (tmp->cols != cols || tmp->rows != rows) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= (operands[i]->matchesForRule(tmp->operands[i], correspondance));
    }

    return b;

}

std::shared_ptr<Expression> MatrixExpression::rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    std::vector<std::shared_ptr<Expression>> nop(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        nop[i] = operands[i]->rebuildWith(correspondance);
    }

    return std::shared_ptr<Expression>(new MatrixExpression(rows, cols, nop));

}

SymbolExpression::SymbolExpression(std::string sym) {
    this->symbol = sym;
    this->neg = false;
}

SymbolExpression::SymbolExpression(std::string sym, bool n) {
    this->symbol = sym;
    this->neg = n;
}

SymbolExpression::~SymbolExpression() {

}

bool SymbolExpression::isConstant() {
    return false;
    //return Expression::getSymbol(symbol) && Expression::getSymbol(symbol)->isConstant();
}

std::ostream & SymbolExpression::writeToStream(std::ostream & stream) {

    if (Configuration::getValue("tex_print")) {
        return stream << this->format();
    }

    return stream << this->symbol;
}

std::shared_ptr<Expression> SymbolExpression::negated(std::shared_ptr<Expression> exp) {

    return std::shared_ptr<Expression>(new SymbolExpression(symbol, !neg));

}

dMatrix<Number> SymbolExpression::getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) {

    if (variables.find(symbol) != variables.end()) {
        return variables[symbol]->getValue(variables) * Number(neg ? -1 : 1);
    }
    std::shared_ptr<Expression> exp = Expression::getSymbol(symbol);
    if (exp)
        return exp->getValue(variables) * Number(neg ? -1 : 1);

    throw std::runtime_error(std::string("Unknown variable ").append(symbol));

}

std::ostream & operator<<(std::ostream & stream, std::shared_ptr<Expression> & exp) {

    return exp->writeToStream(stream);
}

bool SymbolExpression::matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    if (symbol[0] == '$') {

        if (correspondance.find(symbol) != correspondance.end()) {

            std::shared_ptr<Expression> foundExp = correspondance[symbol];
            return exp->matchesForRule(foundExp, correspondance);


        }

        correspondance[symbol] = exp;
        return true;
    }

    std::shared_ptr<SymbolExpression> tmp;
    if (tmp = std::dynamic_pointer_cast<SymbolExpression>(exp)) {

        return !symbol.compare(tmp->symbol) && neg == tmp->neg;

    }

    return false;

}

std::shared_ptr<Expression> SymbolExpression::rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) {

    if (symbol[0] == '$') {

        if (correspondance.find(symbol) != correspondance.end()) {
            if (!neg)
                return correspondance[symbol];

            return correspondance[symbol]->negated(correspondance[symbol]);
        }

        return std::shared_ptr<Expression>(new SymbolExpression(symbol.substr(1, symbol.length()-1), neg));

    }

    return std::shared_ptr<Expression>(new SymbolExpression(symbol, neg));

}

std::string SymbolExpression::format() {

    std::string fsymbol;

    if (hasFormatString(symbol)) {
        fsymbol = getFormatString(symbol);
    } else {
        fsymbol = symbol;
    }

    return std::string(neg ? "-" : "").append(fsymbol);

}

std::shared_ptr<Expression> SymbolExpression::getDefinition() {
    return Expression::getSymbol(this->symbol);
}

std::string SymbolExpression::getSymbolName(){

    return this->symbol;

}
