#ifndef PRINTING_H
#define PRINTING_H

#include <string>
#include <ios>
#include "util/dmatrix.h"
#include "util/number.h"

class Expression;

namespace printing {

std::string formatMatrix(Math::dMatrix<Math::Number> mat);

enum OutputType {

    //Value used for the initial expression in a
    // simplify command
    SIMPLIFY_BEGIN,
    //Used for each expression in a simplify command
    SIMPLIFY_STEP,

    //Used for the given expression for a
    //compute command
    COMPUTE_BEGIN,
    //Used for the result of a compute
    //command
    COMPUTE_RESULT,

    // Used for a show command
    SHOW,
};

void setOutStream(std::ostream & stream);
void setErrStream(std::ostream & stream);

void setPrintingCallback(std::function<void()> f);

void startPrinter();
// start printing to this stream
void startPrinter(std::ostream & stream);
void endPrinter();
void printExpression(std::shared_ptr<Expression> exp, OutputType type);
void printString(std::string str);
void printShow(std::shared_ptr<Expression> exp, std::string symbol);

}

#endif // PRINTING_H
