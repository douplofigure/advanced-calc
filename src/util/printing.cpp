#include "printing.h"

#include <sstream>

#include "configuration.h"
#include "expression.h"

using namespace printing;
using namespace Math;

namespace printing {

std::ostream * printStream = &std::cout;
std::ostream * currentStream = printStream;

std::ostream * errStream = &std::cerr;

std::function<void()> printingCallback;

enum PrinterState {

    S_DISABLED,

    S_INITIAL,

    S_SIMPLIFY_BEG,
    S_SIMPLIFY,

    S_COMPUTE,

    S_END,

};

PrinterState printerState;

std::ostream & stream() {
    return *currentStream;
}

void printExpressionInitial(std::shared_ptr<Expression> exp, OutputType type);
void printExpressionSimplifyBegin(std::shared_ptr<Expression> exp, OutputType type);
void printExpressionSimplify(std::shared_ptr<Expression> exp, OutputType type);
void printExpressionCompute(std::shared_ptr<Expression> exp, OutputType type);

}

std::string printing::formatMatrix(dMatrix<Number> mat) {

    int rows = mat.getRowCount();
    int cols = mat.getColCount();

    std::stringstream stream;

    if (cols == 1) {

        if (rows == 1) {
            Number n = mat(0,0);
            return n.toTeXFormat();
        }

        stream << "\\begin{pmatrix} ";
        for (int i = 0; i < rows; ++i) {
            stream << mat(i, 0).toTeXFormat();
            if (i < rows-1)
                stream << " \\\\ ";
        }
        stream << " \\end{pmatrix}";

        return stream.str();

    }

    stream << "\\begin{pmatrix} ";

    for (int i = 0; i < rows; ++i) {

        for (int j = 0; j < cols; ++j) {

            stream << mat(i, j).toTeXFormat();

            if (j < cols -1)
                stream << " & ";

        }

        if (i < rows-1)
            stream << " \\\\ ";

    }

    stream << "\\end{pmatrix} ";

    return stream.str();

}

void printing::setOutStream(std::ostream & stream) {
    printStream = &stream;
    currentStream = &stream;
}

void printing::setErrStream(std::ostream & stream) {
    errStream = &stream;
}

void printing::setPrintingCallback(std::function<void()> f) {
    printingCallback = f;
}

void printing::startPrinter() {
    startPrinter(*printStream);
}

void printing::startPrinter(std::ostream & stream) {
    currentStream = &stream;
    printerState = S_INITIAL;
}

void printing::printExpressionInitial(std::shared_ptr<Expression> exp, OutputType type) {

    switch (type) {

        case SIMPLIFY_BEGIN:
            if (Configuration::getValue("tex_print")) {
                stream() << "\\begin{align*}" << std::endl;
            }
            stream() << exp << " ";
            printerState = S_SIMPLIFY_BEG;
            break;

        case COMPUTE_BEGIN:
            if (Configuration::getValue("tex_print")) {
                stream() << "\\begin{align*}" << std::endl;
            }
            stream() << exp << " ";
            if (Configuration::getValue("compute_definitions")) {
                std::shared_ptr<Expression> tmp = exp->getDefinition();
                if (tmp) {
                    if (Configuration::getValue("tex_print")) stream() << "&";
                    stream() << "= " << tmp << " \\\\" << std::endl;
                }
            }
            printerState = S_COMPUTE;
            break;

        case SHOW:
            if (Configuration::getValue("tex_print")) {
                stream() << "$" << exp << "$" << std::endl;
            } else {
                stream() << exp << std::endl;
            }
            printerState = S_END;
            break;

        default:
            throw std::runtime_error("Incorrect printer configuration");


    };

}

void printing::printExpressionSimplifyBegin(std::shared_ptr<Expression> exp, OutputType type) {

    if (type != SIMPLIFY_STEP) {
        throw std::runtime_error("Incorrect printer configuration");
    }
    printerState = S_SIMPLIFY;

}

void printing::printExpressionSimplify(std::shared_ptr<Expression> exp, OutputType type) {

    if (type != SIMPLIFY_STEP && type != COMPUTE_BEGIN){
        throw std::runtime_error("Incorrect printer configuration");
    }

    if (type == COMPUTE_BEGIN) {
        printerState = S_COMPUTE;
        return;
    }

    if (Configuration::getValue("tex_print")) stream() << "&";
    stream() << "= " << exp;
    if (Configuration::getValue("tex_print")) stream() << " \\\\ ";
    stream() << std::endl;

}

void printing::printExpressionCompute(std::shared_ptr<Expression> exp, OutputType type) {

    if (type != COMPUTE_RESULT) {
        throw std::runtime_error("Incorrect printer configuration");
    }

    if (Configuration::getValue("tex_print")) stream() << "&";
    stream() << "= " << exp << std::endl;
    if (Configuration::getValue("tex_print"))
        stream() << "\\end{align*}" << std::endl;

    printerState = S_END;

}

void printing::printExpression(std::shared_ptr<Expression> exp, OutputType type) {

    switch (printerState) {

        case S_DISABLED:
            break;

        case S_INITIAL:
            printExpressionInitial(exp, type);
            break;

        case S_SIMPLIFY_BEG:
            printExpressionSimplifyBegin(exp, type);
            break;

        case S_SIMPLIFY:
            printExpressionSimplify(exp, type);
            break;

        case S_COMPUTE:
            printExpressionCompute(exp, type);
            break;

    }

    printingCallback();

}

void printing::endPrinter() {

    switch (printerState) {

        case S_SIMPLIFY:
            if (Configuration::getValue("tex_print"))
                stream() << "\\end{align*}" << std::endl;

        case S_END:
        case S_INITIAL:
            break;

        default:
            throw std::runtime_error("Incorrect printer configuration");
    }

    printerState = S_DISABLED;
}

void printing::printString(std::string str) {
    stream() << str;
    printingCallback();
}

void printing::printShow(std::shared_ptr<Expression> exp, std::string symbol) {
    if (Configuration::getValue("tex_print")) stream() << "$";
    stream() << symbol << " = " << exp;
    if (Configuration::getValue("tex_print")) stream() << "$";
    stream() << std::endl;

    printingCallback();
}
