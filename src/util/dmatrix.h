#ifndef DMATRIX_H
#define DMATRIX_H

#include <functional>

#include "dvector.h"

namespace Math {

template <typename T = double> class dMatrix {

    public:
        dMatrix(unsigned int rows, unsigned int cols) {

            this->rows = rows;
            this->cols = cols;
            this->data = new T[rows*cols];

            for (int i = 0; i < rows * cols; ++i) {
                this->data[i] = (T) (i % (rows + cols + 1)? 1 : 0);
            }

        }

        dMatrix() {
            this->rows = 1;
            this->cols = 1;
            this->data = new T[1];
            data[0] = 0;
        }

        /** Data sorted by rows, It looks correct when you write it:

            (0, 1)
            (1, 0)

        is optained by passing {0, 1, 0, 1}

        */
        dMatrix(unsigned int rows, unsigned int cols, T * data) {

            this->rows = rows;
            this->cols = cols;
            this->data = new T[rows*cols];

            for (int i = 0; i < rows * cols; ++i) {
                this->data[i] = data[i];
            }

        }

        dMatrix(const dMatrix& mat) {
            this->cols = mat.cols;
            this->rows = mat.rows;

            this->data = new T[cols*rows];
            for (int i = 0; i < cols * rows; ++i)
                data[i] = mat.data[i];

        }

        virtual ~dMatrix() {
            delete[] data;
        }

        dVector<T> operator[](int i) {
            if (i < 0 || i >= rows)
                throw std::runtime_error("Matrix index out of bounds");

            return dVector<T>(cols, this->data + i * cols);
        }

        dVector<T> operator()(int i) {
            if (i < 0 || i >= cols)
                throw std::runtime_error("Matrix index out of bounds");
            T tmp[rows];
            for (int j = 0; j < rows; ++j) {
                tmp[j] = data[j*cols + i];
            }
            return dVector<T>(rows, tmp);
        }

        T& operator() (int i, int j) {
            if (i < 0 || i >= rows || j < 0 || j >= cols)
                throw std::runtime_error("Matrix index out of bounds");
            return *(this->data + i * cols + j);
        }

        dVector<T> operator*(dVector<T> vec) {
            if (vec.getDim() != this->cols)
                throw std::runtime_error("Matrix Vector size mismatch");
            T tmp[cols];
            for (int i = 0; i < cols; ++i) {
                tmp[i] = (*this)[i] * vec;
            }
            return dVector<T>(cols, tmp);

        }

        dMatrix<T> operator+(dMatrix<T> mat) {

            if (this->cols != mat.cols || this->rows != mat.rows) {
                throw std::runtime_error("Matrix addition size mismatch");
            }

            dMatrix<T> res(rows, cols);

            for (int i = 0; i < cols * rows; ++i) {
                res.data[i] = data[i] + mat.data[i];
            }

            return res;

        }

        dMatrix<T> operator*(T val) {

            dMatrix<T> res(rows, cols);
            for (int i = 0; i < rows * cols; ++i) {
                res.data[i] = val * data[i];
            }
            return res;

        }

        dMatrix<T> transpose() {

            dMatrix<T> res(cols, rows);

            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < cols; ++j) {
                    T tmp = (*this)(i, j);
                    res(j, i) = tmp;
                }
            }
            return res;

        }

        dMatrix<T> operator*(dMatrix<T> mat) {

            if (this->cols == 1 && this->rows == 1) {
                return mat * (*this)(0,0);
            }

            if (mat.cols == 1) {

                if (mat.rows == 1) {
                    return *this * mat(0,0);
                }

                if (this->cols == 1) {
                    return this->transpose() * mat;
                }

            }

            if (this->cols != mat.rows) {
                throw std::runtime_error("Matrix multiplication size mismatch");
            }

            dMatrix<T> res(rows, mat.cols);

            for (int i = 0; i < res.rows; ++i) {

                for (int j = 0; j < res.cols; ++j) {
                    res(i, j) = ((*this)[i] * mat(j));
                }

            }

            return res;

        }

        bool operator==(dMatrix<T> mat) {

            if (mat.cols != cols || mat.rows != rows)
                return false;

            bool b = true;

            for (int i = 0; i < cols * rows; ++i) {
                b &= (mat.data[i] == data[i]);
            }

            return b;

        }

        dMatrix<T> operator-(dMatrix<T> mat) {

            if (this->cols != mat.cols || this->rows != mat.rows) {
                throw std::runtime_error("Matrix substraction size mismatch");
            }

            dMatrix<T> res(rows, cols);

            for (int i = 0; i < cols * rows; ++i) {
                res.data[i] = data[i] - mat.data[i];
            }

            return res;

        }

        dMatrix<T>& operator=(dMatrix<T> mat) {
            this->cols = mat.cols;
            this->rows = mat.rows;
            if (this->data) {
                delete[] this->data;
            }
            this->data = new T[cols*rows];
            for (int i = 0; i < cols*rows; ++i) {
                data[i] = mat.data[i];
            }
            return *this;
        }

        T det() {
            dMatrix<T> lu = LU();
            T res = (T) 1;
            for (int i = 0; i < cols; ++i) {
                res *= lu[i][i];
            }
            return res;
        }

        dMatrix<T> withColumn(int c, dVector<T> vec) {

            T tmp[rows * cols];
            for (int i = 0; i < rows; ++i) {

                for (int j = 0; j < cols; ++j) {

                    if (j == c) {
                        tmp[i*cols+j] = vec[i];
                    } else {
                        tmp[i*cols+j] = data[i*cols+j];
                    }

                }

            }
            return dMatrix<T>(rows, cols, tmp);

        }

        dVector<T> solve(dVector<T> vec) {

            if (rows == cols) {

                dMatrix<T> lu = LU();

                return this->luSolve(lu, vec);

                return solveKramer(vec);

            } else {
                throw std::runtime_error("Waiting on Gaus-solving");
            }

        }

        unsigned int getRowCount() {
            return rows;
        }

        unsigned int getColCount() {
            return cols;
        }

        dMatrix<T> LU() {

            if (rows != cols)
                throw std::runtime_error("LU only available for square matrices");

            dMatrix<T> res(rows, cols);
            for (int j = 0; j < cols; ++j) {

                for (int i = 0; i < rows; ++i) {

                    if (i <= j) {

                        res(i,j) = (*this)[i][j] - getLUFactor(i, i, j, res);

                    } else {

                        res(i,j) = ((*this)[i][j] - getLUFactor(j, i, j, res)) / res[j][j];

                    }

                }

            }

            return res;

        }

        static dMatrix<T> applyToAll(dMatrix<T> & mat, std::function<T(T)> f) {

            dMatrix<T> res(mat.rows, mat.cols);
            for (int i = 0; i < mat.rows * mat.cols; ++i)
                res.data[i] = f(mat.data[i]);

            return res;

        }

        std::ostream & toStream(std::ostream & stream) {

            if (cols == 1) {

                if (rows == 1) {
                    return stream << data[0];
                }

                return stream << (*this)(0);

            }

            stream << "|";
            stream << (*this)[0];
            for (int i = 1; i < getRowCount(); ++i) {
                stream << "|\n|" << (*this)[i];
            }
            stream << "|";
            return stream;

        }

    protected:

    private:

        dVector<T> solveKramer(dVector<T> vec) {
            T d = det();
            dVector<T> v(cols, nullptr);
            for (int i = 0; i < cols; ++i) {
                v[i] = withColumn(i, vec).det() / d;
            }
            return v;
        }

        T mDet(int r, int c, T * idata) {

            if (r == 2) {
                return idata[0] * idata[3] - idata[1] * idata[2];
            }

            T val = (T) 0;
            for (int i = 0; i < c; ++i) {
                T * tmp = mForDet(r, c, i, idata);
                val += (i % 2 ? -1 : 1) * idata[i] * mDet(r-1, c-1, tmp);
                delete[] tmp;
            }
            return val;

        }

        T * mForDet(int r, int c, int k, T * iData) {

            T * tmp = new T[(r-1) * (c-1)];
            int index = 0;
            for (int i = 1; i < r; ++i) {
                for (int j = 0; j < c; ++j) {
                    if (k != j)
                        tmp[index++] = iData[i*r+j];
                }

            }
            return tmp;

        }

        T getLUFactor(int m, int i, int j, dMatrix<T> mat) {

            T res = (T) 0;
            for (int k = 0; k < m; ++k) {

                res += (k == i ? 1.0 : mat[i][k]) * mat[k][j];

            }

            return res;

        }

        dVector<T> luSolve(dMatrix<T> lu, dVector<T>& vec) {

            dVector<T> y(cols);
            y[0] = vec[0];
            for (int i = 1; i < cols; ++i) {
                T t = (T) 0;
                for (int j = 0; j < i; ++j) {
                    t += (i == j ? 1 : lu[i][j]) * y[j];
                }
                y[i] = vec[i] - t;
            }

            dVector<T> x(cols);
            x[cols-1] = y[cols-1] / lu[cols-1][cols-1];
            for (int i = cols-1; i >= 0; --i) {
                T t = (T) 0;
                for (int j = i + 1; j < cols; ++j)
                    t += lu[i][j] * x[j];
                x[i] = (1 / lu[i][i]) * (y[i] - t);

            }

            return x;

        }

        T * data;
        unsigned int rows;
        unsigned int cols;

};

}

template <typename T> inline std::ostream& operator<<(std::ostream& stream, Math::dMatrix<T> mat) {
    mat.toStream(stream);
}
#endif // DMATRIX_H
