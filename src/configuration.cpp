#include "configuration.h"

namespace Configuration {

    std::unordered_map<std::string, long int> data;
    bool output;

long int getValue(std::string name) {
    if (data.find(name) == data.end()) {
        throw std::runtime_error("Could not find configuration variable.");
    }

    return data[name];

}

void setValue(std::string name, long int value) {

    data[name] = value;

}

void setDefaultValues() {

    setValue("tex_print", 0);
    setValue("compute_definitions", 0);
    setValue("print_precision", 6);

}

void enableOutput() {

    output = true;

}

bool outputEnabled() {
    return output;
}

void disableOutput() {
    output = false;
}

}
