#include "api.h"
#include <cstring>
#include <fstream>

namespace calc {

    std::queue<char> theQueue;

}


#include "operator.h"
#include "expression.h"
#include "configuration.h"

using namespace calc;

extern "C" {

#include "notation.parser.h"

//YYSTYPE symlval;

int symparse();

int processLexeme(char * str) {

    std::string symbol(str);

    symlval.symbol = (char*) malloc((symbol.length() + 1) * sizeof(char));
    strcpy(symlval.symbol, symbol.c_str());
    symlval.symbol[symbol.length()] = 0;

    if (Operator::isOperator(symbol)) {
        if (symbol[0] == '-')
            return OPERATOR_MINUS;
        return OPERATOR;
    }

    return SYMBOL;

}

int getLexerInput(char * buffer, int max_size) {

    if (theQueue.empty()) {
        buffer[0] = -1;
        return 0;
    }

    buffer[0] = theQueue.front();
    buffer[1] = 0;
    theQueue.pop();
    return 1;

}

}

std::queue<char>& operator<<(std::queue<char>& q, std::string str) {
    for (int i = 0; i < str.length(); ++i) {
        q.push(str[i]);
    }
    return q;
}

void calc::init() {

    Function::loadBuiltins();
    Configuration::setDefaultValues();

}

void calc::parseFile(const char * fname) {

    std::ifstream file;

    file.open(fname);

    while (!file.eof()) {

        std::string line;
        std::getline(file, line);

        theQueue << line;

    }

    symparse();

}

void calc::parseString(std::string str, bool addSemicolon) {
    theQueue << str;
    if (addSemicolon)
        theQueue << ";";
    symparse();
}

void calc::clearBuffer() {
    while (!theQueue.empty())
        theQueue.pop();
}

void calc::executeBuffer() {

    symparse();

}

void calc::pushToBuffer(std::string str) {
    theQueue << str;
}
