/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_SYM_SRC_NOTATION_PARSER_H_INCLUDED
# define YY_SYM_SRC_NOTATION_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int symdebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    KW_LET = 258,
    CONST_FLOAT = 259,
    CONST_INT = 260,
    OP_DEF = 261,
    SYMBOL = 262,
    OPERATOR = 263,
    OPERATOR_MINUS = 264,
    KW_COMPUTE = 265,
    KW_WITH = 266,
    KW_EQUATION = 267,
    KW_SOLVE = 268,
    KW_RULE = 269,
    OP_TO = 270,
    KW_SIMPLIFY = 271,
    KW_SHOW = 272,
    KW_CONFIG = 273,
    KW_EXIT = 274,
    KW_APPLY = 275
  };
#endif
/* Tokens.  */
#define KW_LET 258
#define CONST_FLOAT 259
#define CONST_INT 260
#define OP_DEF 261
#define SYMBOL 262
#define OPERATOR 263
#define OPERATOR_MINUS 264
#define KW_COMPUTE 265
#define KW_WITH 266
#define KW_EQUATION 267
#define KW_SOLVE 268
#define KW_RULE 269
#define OP_TO 270
#define KW_SIMPLIFY 271
#define KW_SHOW 272
#define KW_CONFIG 273
#define KW_EXIT 274
#define KW_APPLY 275

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 4 "src/notation.y" /* yacc.c:1909  */


    double d;
    double img;
    long int i;

    char * symbol;
    void * exp;


#line 105 "src/notation.parser.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE symlval;

int symparse (void);

#endif /* !YY_SYM_SRC_NOTATION_PARSER_H_INCLUDED  */
