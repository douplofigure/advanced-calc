#ifndef OPERATOR_H
#define OPERATOR_H

#include <unordered_map>
#include <string>
#include <memory>
#include <vector>
#include <functional>

#include "function.h"

#include "util/dmatrix.h"
#include "util/number.h"

class Expression;

typedef std::function<std::shared_ptr<Expression>(std::shared_ptr<FunctionExpression>)> NegationFunction;

class Operator : public Function
{
    public:
        Operator(std::string symbol, unsigned int p);
        Operator(std::string symbol, unsigned int p, std::string formatSymbol);
        virtual ~Operator();

        //virtual Math::dMatrix<Math::Number> evaluate(std::vector<Math::dMatrix<Math::Number>> args) = 0;

        static Operator * getOperator(std::string symbol);
        static bool isOperator(std::string symbol);

        virtual std::ostream & writeToStream(std::ostream & stream, std::vector<std::shared_ptr<Expression>> & operands);
        virtual unsigned int getPrecedence();

        std::string format(const std::vector<std::shared_ptr<Expression>> & operands);

    protected:

    private:

        std::string symbol;
        std::string formatSymbol;
        unsigned int precedence;

        static std::unordered_map<std::string, std::unique_ptr<Operator>> definedOperators;

};

class BuiltinOperator : public Operator {

    public:
        BuiltinOperator(std::string symbol, unsigned int p, ComputeFunction operation);
        BuiltinOperator(std::string symbol, unsigned int p, std::string formatSymbol, ComputeFunction operation);
        virtual ~BuiltinOperator();

        int getComplexity();

        void setNegationFunction(NegationFunction f);
        std::shared_ptr<Expression> negateExpression(std::shared_ptr<FunctionExpression> exp);

    protected:
        virtual Math::dMatrix<Math::Number> evaluate(std::vector<std::shared_ptr<Expression>> args, std::unordered_map<std::string, std::shared_ptr<Expression>> & symbolTable);

    private:
        bool hasNegation;
        ComputeFunction operation;
        NegationFunction negationFunc;

};

class DivisionOperator : public BuiltinOperator {

    public:
        using BuiltinOperator::BuiltinOperator;

        std::string format(const std::vector<std::shared_ptr<Expression>> & operands);
};

#endif // OPERATOR_H
