#include "cfunctions.h"

#include <memory>
#include <iostream>
#include <limits>
#include <chrono>

#include "expression.h"
#include "operator.h"
#include "rule.h"
#include "configuration.h"
#include "util/printing.h"

struct ExpCapsule {

    std::shared_ptr<Expression> exp;

};

struct ExpList {
    std::vector<std::shared_ptr<Expression>> exps;
};

struct ExpMatrix {
    std::vector<std::vector<std::shared_ptr<Expression>>> exps;
};

void * expressionCreateInt(long int value) {

    std::shared_ptr<ConstantExpression> exp(new ConstantExpression(value));
    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    return (void*) capsule;

}

void * expressionCreateDouble(double value) {

    std::shared_ptr<ConstantExpression> exp(new ConstantExpression(value));
    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    return (void*) capsule;

}

void * expressionCreateSymbol(char * symbol) {

    std::shared_ptr<SymbolExpression> exp(new SymbolExpression(std::string(symbol)));
    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    free(symbol);

    return (void *) capsule;

}

void * expressionCreateVector(void * tmp) {

    ExpList * ls = (ExpList *) tmp;

    std::shared_ptr<VectorExpression> exp(new VectorExpression(ls->exps));
    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    delete ls;

    return (void *) capsule;

}

void * expressionCreateMatrix(void * mat) {

    ExpMatrix * m = (ExpMatrix *) mat;

    std::shared_ptr<MatrixExpression> exp(new MatrixExpression(m->exps));
    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    delete m;

    return (void *) capsule;

}

void * expressionCreateFunction(char * iName, void * tmp) {

    std::string name(iName);
    ExpList * ls = (ExpList *) tmp;

    Function * f = Function::getFunction(name);
    std::shared_ptr<FunctionExpression> res(new FunctionExpression(f, ls->exps));

    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = res;

    delete ls;
    free(iName);

    return (void *) capsule;

}

void * expressionMarkParenthesis(void * exp) {

    ExpCapsule * e = (ExpCapsule *) exp;

    e->exp->setPrecedenceOverride(std::numeric_limits<unsigned short int>::max());

    return exp;

}

void * expressionNegate(void * exp) {

    ExpCapsule * capsule = (ExpCapsule *) exp;

    std::shared_ptr<Expression> resExp = capsule->exp->negated(capsule->exp);
    if (resExp->getPrecedence() < 3)
        resExp->setPrecedenceOverride(3);

    delete capsule;

    ExpCapsule * res = new ExpCapsule();
    res->exp = resExp;

    return (void *) res;

}

void * expressionJoinOperator(char * opName, void * tmp1, void * tmp2) {

    ExpCapsule * exp1 = (ExpCapsule*) tmp1;
    ExpCapsule * exp2 = (ExpCapsule*) tmp2;

    Operator * op = Operator::getOperator(std::string(opName));
    std::vector<std::shared_ptr<Expression>> operands(2);
    operands[0] = std::shared_ptr<Expression>(exp1->exp);
    operands[1] = std::shared_ptr<Expression>(exp2->exp);

    if (op->getPrecedence() >= operands[1]->getPrecedence()) {

        std::shared_ptr<FunctionExpression> fexp = std::dynamic_pointer_cast<FunctionExpression>(operands[1]);

        std::vector<std::shared_ptr<Expression>> & oldOps = fexp->getOperands();

        std::vector<std::shared_ptr<Expression>> newOps(2);
        newOps[0] = operands[0];
        newOps[1] = oldOps[0];

        operands[0] = std::shared_ptr<FunctionExpression>(new FunctionExpression(op, newOps));

        operands[1] = oldOps[1];

        op = (Operator *) fexp->getOperator();

    }

    std::shared_ptr<FunctionExpression> res(new FunctionExpression(op, operands));

    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = res;

    delete exp1;
    delete exp2;

    free(opName);

    return (void *) capsule;

}

void symbolDeclare(char * sName, void * tmp) {

    ExpCapsule * exp = (ExpCapsule *) tmp;

    std::string name(sName);

    Expression::declareSymbol(name, exp->exp);

    delete exp;
    free(sName);

}

void * symbolShow(char * symbol) {

    std::string name(symbol);

    free(symbol);

    std::shared_ptr<Expression> exp = Expression::getSymbol(name);

    printing::printShow(exp, name);

    ExpCapsule * capsule = new ExpCapsule();
    capsule->exp = exp;

    return (void *) capsule;

}

struct VariableList {

    std::unordered_map<std::string,std::shared_ptr<Expression>> m;

};

void * variableListBegin(char * symbol, void * exp) {

    std::string name(symbol);
    ExpCapsule * capsule = (ExpCapsule *) exp;
    std::shared_ptr<Expression> e = capsule->exp;

    VariableList * ls = new VariableList();
    ls->m[name] = e;

    delete capsule;
    free(symbol);

    return (void *) ls;

}

void * variableListAppend(void * tmp, char * symbol, void * exp) {

    std::string name(symbol);
    ExpCapsule * capsule = (ExpCapsule *) exp;
    std::shared_ptr<Expression> e = capsule->exp;

    VariableList * ls = (VariableList *) tmp;
    ls->m[name] = e;

    delete capsule;
    free(symbol);

    return (void *) ls;

}

void * expressionCompute(void * exp, void * tmpLs) {

    ExpCapsule * capsule = (ExpCapsule *) exp;
    std::shared_ptr<Expression> e = capsule->exp;

    auto startTimeP = std::chrono::high_resolution_clock::now();
    std::unordered_map<std::string, std::shared_ptr<Expression>> m;

    printing::printExpression(e, printing::COMPUTE_BEGIN);

    if (tmpLs) {
        VariableList * ls = (VariableList *) tmpLs;
        m = ls->m;
        delete ls;
    }

    auto res = e->getValue(m);

    std::shared_ptr<Expression> tmp(new ConstantExpression(res));

    printing::printExpression(tmp, printing::COMPUTE_RESULT);

    auto endTimeP = std::chrono::high_resolution_clock::now();

    delete capsule;

    ExpCapsule * retVal = new ExpCapsule();
    retVal->exp = tmp;

    return (void *) retVal;

}

void * expressionListBegin(void * exp) {

    ExpCapsule * e = (ExpCapsule *) exp;

    ExpList * ls = new ExpList();
    ls->exps.push_back(e->exp);

    delete e;

    return ls;

}

void * expressionListAppend(void * tmp, void * exp) {

    ExpCapsule * e = (ExpCapsule *) exp;

    ExpList * ls = (ExpList *) tmp;
    ls->exps.push_back(e->exp);

    delete e;

    return ls;

}

void * expressionMatrixBegin(void * line) {

    ExpList * ls = (ExpList *) line;

    ExpMatrix * mat = new ExpMatrix();
    mat->exps.push_back(ls->exps);

    delete ls;

    return (void *) mat;

}

void * expressionMatrixAppend(void * mat, void * line) {

    ExpList * ls = (ExpList *) line;
    ExpMatrix * m = (ExpMatrix *) mat;

    if (ls->exps.size() != m->exps[0].size())
        throw std::runtime_error("Lines of different lengths in matrix declaration.");

    m->exps.push_back(ls->exps);

    delete ls;

    return (void *) m;

}

void * ruleCreate(void * head, void * tail) {

    ExpCapsule * h = (ExpCapsule *) head;
    ExpCapsule * t = (ExpCapsule *) tail;

    Rule * r = new Rule(h->exp, t->exp);

    delete h;
    delete t;

    return r;

}

void rulesApply(void * exp) {

    ExpCapsule * e = (ExpCapsule *) exp;

    Rule::applyAll(e->exp);

    delete e;

}

void * expressionSimplify(void * tmp){

    ExpCapsule * capsule = (ExpCapsule *) tmp;
    std::shared_ptr<Expression> exp = capsule->exp;

    printing::printExpression(exp, printing::SIMPLIFY_BEGIN);

    delete capsule;

    std::shared_ptr<Expression> prev = exp;

    do {

        printing::printExpression(exp, printing::SIMPLIFY_STEP);

        prev = exp;

        std::list<std::shared_ptr<Expression>> ls = exp->step(exp);

        for (std::shared_ptr<Expression> e : ls) {
            printing::printExpression(e, printing::SIMPLIFY_STEP);
        }

        if (ls.size())exp = ls.back();
        else exp = nullptr;

    } while (exp);

    ExpCapsule * c = new ExpCapsule();
    c->exp = prev;

    return (void *) c;

}

void configSetValue(char * name, long int value) {

    std::string n(name);

    Configuration::setValue(n, value);

    free(name);

}

void configEnableOutput() {
    Configuration::enableOutput();
    printing::startPrinter();
}

void configDisableOutput() {
    Configuration::disableOutput();
    printing::endPrinter();
}

void messagePrint(char * msg) {

    std::string str(msg);

    str = str.substr(1, str.length()-2);

    std::cout << str << std::endl;

}

void functionDeclare(char * symbol, void * args, void * exp) {

    std::string n(symbol);
    ExpList * ls = (ExpList *) args;
    ExpCapsule * capsule = (ExpCapsule *) exp;

    DefinedFunction * f = new DefinedFunction(n, ls->exps, capsule->exp);

    delete ls;
    delete capsule;

}
