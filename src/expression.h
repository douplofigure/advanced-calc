#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <list>

#include "util/dmatrix.h"
#include "util/number.h"

class Rule;

class Function;

template <typename T> std::unordered_set<T> & operator+=(std::unordered_set<T> & s1, const std::unordered_set<T> s2);

class Expression
{
    public:
        Expression();
        virtual ~Expression();

        virtual bool isConstant() = 0;
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables) = 0;

        virtual std::ostream & writeToStream(std::ostream & stream) = 0;

        virtual unsigned int getPrecedence();
        virtual void setPrecedenceOverride(unsigned int p);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance) = 0;

        virtual int getComplexity();
        virtual std::unordered_set<std::shared_ptr<Expression>> getNeighbors(Rule * rule, std::shared_ptr<Expression> exp);

        virtual std::string format();
        virtual std::shared_ptr<Expression> negated(std::shared_ptr<Expression> exp);

        virtual std::shared_ptr<Expression> getDefinition();

        static void declareSymbol(std::string name, std::shared_ptr<Expression> exp);
        static void declareSymbol(std::string name, std::shared_ptr<Expression> exp, std::string format);

        static void declareConstant(std::string name, Math::Number n);
        static void declareConstant(std::string name, Math::dMatrix<Math::Number> n);
        static void declareConstant(std::string name, Math::Number n, std::string format);
        static void declareConstant(std::string name, Math::dMatrix<Math::Number> n, std::string format);

        static std::shared_ptr<Expression> getSymbol(std::string name);

        std::list<std::shared_ptr<Expression>> step(std::shared_ptr<Expression> exp);

    protected:

        static bool hasFormatString(std::string name);
        static std::string getFormatString(std::string name);

    private:

        static std::unordered_map<std::string, std::shared_ptr<Expression>> symbolTable;
        static std::unordered_map<std::string, std::string> formatTable;

};

class ConstantExpression : public Expression {

    public:
        ConstantExpression(long int i);
        ConstantExpression(double d);
        ConstantExpression(Math::Number n);
        ConstantExpression(Math::dMatrix<Math::Number> n);
        virtual ~ConstantExpression();

        virtual bool isConstant();
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables);

        virtual std::ostream & writeToStream(std::ostream & stream);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::string format();

        virtual std::shared_ptr<Expression> negated(std::shared_ptr<Expression> exp);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);

    private:
        Math::dMatrix<Math::Number> value;

};

class FunctionExpression : public Expression {

    public:
        FunctionExpression(Function * op, std::vector<std::shared_ptr<Expression>> operands);
        virtual ~FunctionExpression();

        virtual bool isConstant();
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables);

        virtual unsigned int getPrecedence();
        virtual void setPrecedenceOverride(unsigned int p);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);

        std::vector<std::shared_ptr<Expression>> & getOperands();
        Function * getOperator();

        virtual int getComplexity();

        virtual std::shared_ptr<Expression> negated(std::shared_ptr<Expression> exp);

        virtual std::unordered_set<std::shared_ptr<Expression>> getNeighbors(Rule * rule, std::shared_ptr<Expression> exp);

        virtual std::ostream & writeToStream(std::ostream & stream);
        virtual std::string format();

    private:

        unsigned int precendenceOverride;

        Function * op;
        std::vector<std::shared_ptr<Expression>> operands;

};

class VectorExpression : public Expression {

    public:
        VectorExpression(std::vector<std::shared_ptr<Expression>> operands);
        virtual ~VectorExpression();

        virtual bool isConstant();
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables);

        virtual std::ostream & writeToStream(std::ostream & stream);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);

        virtual int getComplexity();
        virtual std::unordered_set<std::shared_ptr<Expression>> getNeighbors(Rule * rule, std::shared_ptr<Expression> exp);
        virtual std::string format();

    private:

        std::vector<std::shared_ptr<Expression>> operands;

};

class MatrixExpression : public Expression {

    public:
        MatrixExpression(std::vector<std::vector<std::shared_ptr<Expression>>> operands);
        virtual ~MatrixExpression();

        virtual bool isConstant();
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables);

        virtual std::ostream & writeToStream(std::ostream & stream);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);

        virtual int getComplexity();
        virtual std::unordered_set<std::shared_ptr<Expression>> getNeighbors(Rule * rule, std::shared_ptr<Expression> exp);

        virtual std::string format();

    private:

        MatrixExpression(int rows, int cols, std::vector<std::shared_ptr<Expression>> operands);

        int cols;
        int rows;
        std::vector<std::shared_ptr<Expression>> operands;

};

class SymbolExpression : public Expression {

    public:
        SymbolExpression(std::string symbol);
        SymbolExpression(std::string symbol, bool neg);
        virtual ~SymbolExpression();

        virtual bool isConstant();
        virtual Math::dMatrix<Math::Number> getValue(std::unordered_map<std::string, std::shared_ptr<Expression>> & variables);

        virtual std::ostream & writeToStream(std::ostream & stream);

        virtual bool matchesForRule(std::shared_ptr<Expression> exp, std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);
        virtual std::shared_ptr<Expression> rebuildWith(std::unordered_map<std::string, std::shared_ptr<Expression>> & correspondance);

        virtual std::string format();

        std::string getSymbolName();

        virtual std::shared_ptr<Expression> negated(std::shared_ptr<Expression> exp);

        std::shared_ptr<Expression> getDefinition();

    private:
        bool neg;
        std::string symbol;

};

std::ostream & operator<<(std::ostream & stream, std::shared_ptr<Expression> & exp);

#endif // EXPRESSION_H
