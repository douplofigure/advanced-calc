#include "operator.h"

#include "expression.h"

#include <sstream>

std::unordered_map<std::string, std::unique_ptr<Operator>> Operator::definedOperators;

Operator::Operator(std::string symbol, unsigned int p) : Function(symbol, 2, false) {

    definedOperators[symbol] = std::unique_ptr<Operator>(this);
    this->symbol = symbol;
    this->formatSymbol = symbol;
    this->precedence = p;

}

Operator::Operator(std::string symbol, unsigned int p, std::string fSymbol) : Function(symbol, 2, fSymbol, false) {

    definedOperators[symbol] = std::unique_ptr<Operator>(this);
    this->symbol = symbol;
    this->precedence = p;
    this->formatSymbol = fSymbol;

}


Operator::~Operator()
{
    //dtor
}

Operator * Operator::getOperator(std::string symbol) {

    if (definedOperators.find(symbol) == definedOperators.end())
        throw std::runtime_error(std::string("No such operator ").append(symbol));

    return definedOperators[symbol].get();
}

unsigned int Operator::getPrecedence() {
    return precedence;
}

bool Operator::isOperator(std::string symbol) {
    return definedOperators.find(symbol) != definedOperators.end();
}

std::ostream & Operator::writeToStream(std::ostream & stream, std::vector<std::shared_ptr<Expression>> & operands) {
    return stream << '(' << operands[0] << symbol << operands[1] << ')';
}

std::string Operator::format(const std::vector<std::shared_ptr<Expression>> & operands) {

    if (hasSpecialFormat) {
        return Function::format(operands);
    }
    std::stringstream stream;
    stream << "\\left (" << operands[0]->format() << " " << formatSymbol << " " << operands[1]->format() << "\\right )";
    return stream.str();
}

BuiltinOperator::BuiltinOperator(std::string symbol, unsigned int p, ComputeFunction op) : Operator(symbol, p), operation(op) {

    hasNegation = false;

}

BuiltinOperator::BuiltinOperator(std::string symbol, unsigned int p, std::string fSymbol, ComputeFunction op) : Operator(symbol, p, fSymbol), operation(op) {

    hasNegation = false;

}

BuiltinOperator::~BuiltinOperator(){

}

Math::dMatrix<Math::Number> BuiltinOperator::evaluate(std::vector<std::shared_ptr<Expression>> args, std::unordered_map<std::string, std::shared_ptr<Expression>> & symbolTable) {

    return this->operation(args, symbolTable);

}

void BuiltinOperator::setNegationFunction(NegationFunction f) {

    this->hasNegation = true;
    this->negationFunc = f;

}

std::shared_ptr<Expression> BuiltinOperator::negateExpression(std::shared_ptr<FunctionExpression> exp) {

    if (!this->hasNegation) {
        return nullptr;
    }

    return negationFunc(exp);

}

std::string DivisionOperator::format(const std::vector<std::shared_ptr<Expression>> & operands) {
    std::stringstream stream;

    stream << "\\frac{" << operands[0]->format() << "}{" << operands[1]->format() << "}";
    return stream.str();
}

int BuiltinOperator::getComplexity() {
    return getPrecedence();
}
