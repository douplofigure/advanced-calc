
%name-prefix "sym"

%union {

    double d;
    double img;
    long int i;

    char * symbol;
    void * exp;

}

%{
#include "cfunctions.h"
#include <stdlib.h>
#include <stdio.h>

extern int symlex();
void symerror(char * msg);

%}

%token KW_LET
%token<d> CONST_FLOAT
%token<i> CONST_INT
%token OP_DEF
%token<symbol> SYMBOL
%token<symbol> OPERATOR
%token<symbol> OPERATOR_MINUS
%token KW_COMPUTE
%token KW_WITH
%token KW_EQUATION
%token KW_SOLVE
%token KW_RULE
%token OP_TO
%token KW_SIMPLIFY
%token KW_SHOW
%token KW_CONFIG
%token KW_EXIT
%token KW_APPLY

%type<exp> expression
%type<exp> tempVarList
%type<exp> expList
%type<exp> expVector
%type<exp> expMatrix
%type<exp> vector
%type<exp> matrix
%type<exp> rule
%type<exp> simplify
%type<exp> computation
%type<symbol> operator

%%

script:
    main KW_EXIT ';'        {exit(0);}
    | main
    ;

main:
    %empty
    | main declaration ';'
    | main computation ';'
    | main rule ';'
    | main apply ';'
    | main simplify ';'
    | main config ';'
    | main KW_SHOW {configEnableOutput();} computation ';' {configDisableOutput();}
    | main KW_SHOW {configEnableOutput();} simplify ';' {configDisableOutput();}
    | main KW_SHOW {configEnableOutput();} declaration ';' {configDisableOutput();}
    | main KW_SHOW {configEnableOutput();} SYMBOL ';' {symbolShow($4);configDisableOutput();}
    ;

declaration:
    KW_LET SYMBOL OP_DEF expression  {symbolDeclare($2, $4);}
    | KW_LET SYMBOL '(' expList ')' OP_DEF expression {functionDeclare($2, $4, $7);}
    ;

rule:
    KW_RULE ':' expression OP_TO expression  {$$ = ruleCreate($3, $5);}
    ;

apply:
    KW_APPLY expression          {rulesApply($2);}
    ;

simplify:
    KW_SIMPLIFY expression       {$$ = expressionSimplify($2);}
    ;

config:
    KW_CONFIG SYMBOL OP_DEF CONST_INT   {configSetValue($2, $4);}
    ;

expression:
    CONST_FLOAT                             {$$ = expressionCreateDouble($1);}
    |CONST_INT                              {$$ = expressionCreateInt($1);}
    |expression operator expression         {$$ = expressionJoinOperator($2, $1, $3);}
    |'(' expression ')'                     {$$ = expressionMarkParenthesis($2);}
    |OPERATOR_MINUS '(' expression ')'      {$$ = expressionNegate(expressionMarkParenthesis($3));}
    | SYMBOL                                {$$ = expressionCreateSymbol($1);}
    | OPERATOR_MINUS SYMBOL                 {$$ = expressionNegate(expressionCreateSymbol($2));}
    | vector                                {$$ = $1;}
    | matrix                                {$$ = $1;}
    | OPERATOR_MINUS vector                 {$$ = expressionNegate($2);}
    | OPERATOR_MINUS matrix                 {$$ = expressionNegate($2);}
    | SYMBOL '(' expList ')'                {$$ = expressionCreateFunction($1, $3);}
    | OPERATOR_MINUS SYMBOL '(' expList ')' {$$ = expressionNegate(expressionCreateFunction($2, $4));}
    | simplify                              {$$ = $1;}
    ;

operator:
    OPERATOR                            {$$ = $1;}
    | OPERATOR_MINUS                    {$$ = $1;}
    ;

expList:
    expression                          {$$ = expressionListBegin($1);}
    |expList ',' expression             {$$ = expressionListAppend($1, $3);}
    ;

expVector:
    expression                          {$$ = expressionListBegin($1);}
    |expVector ';' expression           {$$ = expressionListAppend($1, $3);}
    ;

expMatrix:
    expList                             {$$ = expressionMatrixBegin($1);}
    |expMatrix ';' expList              {$$ = expressionMatrixAppend($1, $3);}
    ;

computation:
    KW_COMPUTE expression KW_WITH tempVarList   {$$ = expressionCompute($2, $4);}
    |KW_COMPUTE expression                      {$$ = expressionCompute($2, NULL);}
    ;

tempVarList:
    SYMBOL OP_DEF expression                    {$$ = variableListBegin($1, $3);}
    | tempVarList ',' SYMBOL OP_DEF expression  {$$ = variableListAppend($1, $3, $5);}
    ;

vector:
    '[' expVector ']'                             {$$ = expressionCreateVector($2);}
    ;

matrix:
    '[' expMatrix ']'                             {$$ = expressionCreateMatrix($2);}
    ;

%%

void symerror(char * msg) {
    printf("%s\n", msg);
}

int symwrap() {
    return 1;
}
